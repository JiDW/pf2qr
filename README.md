Pathfinder 2e Quick Rolls

<h1>Auto-Saves & Attacks:</h1>
<p>Automated saves and attacks can be performed by targeting a token using the target tool (easy targets and deselect modules recomended but not necessary while I work on a solution within pf2qr).</p>
<img src='readme-resources/auto-targets.gif'/>
<img src='readme-resources/auto-saves.gif'/>
<p>This is useful for speeding up the gameplay, and comes with several config options:</p>

<h4> Excess Values: </h4>
<p>An excess value can be toggled on and off in the config, describing the value by which a roll beat or missed the DC.</p>
<img src='readme-resources/excess-value.png'/>

<h4> Player Permissions: </h4>
<p>Options are available to control whether the messages are shown to players.</p>
<p>Furthermore, options are in place to control whether a player must be on their turn of combat (if combat is enabled) to make an auto-roll, or if they can auto-roll at all!</p>

<h4> Chat Bubbles: </h4>
<p>These activate panning as a side effect, and can be disabled in the config. When enabled, these create a popup above each targeted token, describing the success level for easier visualization.</p>

<h1> Quick Targeting: </h1>
<p>Once a quick save or attack has been made, the option is available to selectively target either individual tokens or categories of damage, or a mix.</p>
<img src='readme-resources/quick-targeting.gif'/>
<p>This quick targeting helps when each success level experiences different damage types, or for targetting that just that one goblin out of 20.</p>

<h1> Auto-Templates: </h1>
<p>As well as automating attack rolls and spell saves, PF2QR adds the ability to quickly drag and drop templates from spell cards.</p>
<img src='readme-resources/spell-template.gif'/>

<p>Please note: The base pathfinder 2e system does not have template types inputted into all spells. If a spell does not have a template, the spells details can be edited to add the functionality:</p>
<img src='readme-resources/spell-template-config.gif'/>

<h1> Installation </h1>
PF2QR can be installed through the foundry installer, either by search for the module or inputting the following module.json:
Manifest: https://gitlab.com/mcarthur.alford/pf2qr/raw/master/module.json
